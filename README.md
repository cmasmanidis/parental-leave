# parental-leave

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

# Use the app
First click on SGI and/or Low for both parent.
Then select a date.
Now you can select and unselect days to prolong your potential parental leave.
Select day/column for every day in that month.
Select week/row for the whole week.
Select the +/- sign for the whole month
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

