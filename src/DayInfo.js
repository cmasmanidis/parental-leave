export class DayInfo {
    constructor(date, parent1, parent2, sgi ) {
        this.year = date.getFullYear();
        this.month = date.getMonth();
        this.dateNo = date.getDate();
        this.dayOfWeek = date.getDay() === 0 ? 7 : date.getDay(),
        this.parent1 = parent1;
        this.parent2 = parent2;
        this.sgi = sgi;
        this.leave = 0;
        this.weekNo = this.getWeekNo(date);
    } 

    getWeekNo = (day) => {
        let date =  new Date(day.getTime());
        date.setHours(0,0,0,0);
        date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7 );
        const week1 = new Date(date.getFullYear(), 0, 4);            
        return 1 + Math.round( ((date.getTime() - week1.getTime()) / 86400000 -3 + (week1.getDay() + 6) % 7)/7 )
    }
}